package action;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
 
@ManagedBean(name="cuenta")
@SessionScoped
public class CuentaBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String localeCode;	
	private static Map<String,String> cuentas;
	
	public CuentaBean(Map<String,String> cuentas)
	{
		this.cuentas = cuentas;
	}
	

	public static Map<String, String> getCuentas() {
		return cuentas;
	}


	public static void setCuentas(Map<String, String> cuentas) {
		CuentaBean.cuentas = cuentas;
	}


	public CuentaBean()
	{
		cuentas = new HashMap<String,String>();
		cuentas.put("Sin selecci�n", "Sin selecci�n");
	}
	
	
	public void cuentaLocaleCodeChanged(ValueChangeEvent e)
	{
		//cuentas = m_gestorServicios.getMapCuentas(e.getNewValue().toString());
		localeCode = e.getNewValue().toString();
	}
	
	
	public Map<String,String> getCuentaInMap(GestorClientes miGestorServ, String nomCliente) 
	{
		
		if(cuentas == null)
		{
			cuentas = new HashMap<String,String>();
			cuentas.put("Sin selecci�n", "Sin selecci�n");
		}
			
		cuentas.clear();
		return cuentas;
	}

	public String getLocaleCode() {
		return localeCode;
	}
	
	public Map<String,String> getCuentaInMap() 
	{
		return this.cuentas;
	}

	
	
	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

}