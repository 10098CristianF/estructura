package action;


import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.opensymphony.xwork2.ActionSupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ItemsBean extends ActionSupport{
  private List<String> items = new ArrayList<>();
  private List<String> cuentas = new ArrayList<>();
  private String selectedItem;
  GestorClientes lista;
  private static final long serialVersionUID = 1L;
  private GestorClientes m_gestorClientes;	
  
  String cuentaStr = "Sin selección";

  @PostConstruct
  public void postInit() 
  {
	  m_gestorClientes = new GestorClientes(); 
		//System.out.println(m_gestorClientes.getLista().toString());
	  items = m_gestorClientes.getLista();
  }
  //EXTRACCI�N DE DATOS DE CLIENTES.

	public String execute()  throws Exception{
	  cuentas = new ArrayList<String>();
	  
	 //cuentas.add("JUAN");
	  //cuentas.add("OSCAR");
	 // cuentas.add("MAYA");
	  //cuentas.add("dsfsd");
	  cuentas = m_gestorClientes.getMapCuentasArray(selectedItem);
	  String listString = String.join("\n ", cuentas);
	  //return listString;
	  System.out.println(cuentas);
	  return "Success";
	}
  public String getSelectedItem() 
  {   
	  if(selectedItem != null)
	  {
		  cuentas = m_gestorClientes.getMapCuentasArray(selectedItem);
		  if(cuentas.size() >0)
			  cuentaStr = cuentas.get(0);
		  else
			  cuentaStr = "Sin selección";
	  }
      return selectedItem;
  }

  public void setSelectedItem(String selectedItem) 
  {
      this.selectedItem = selectedItem;
  }
  


  
  public List<String> getItems() {
      return items;
  }
  
  public List<String> getCuentas() {
      return cuentas;
  }

  public String getCuentaStr() {
      return cuentaStr;
  }
  
  public void handleChangeB() 
  {  
  }  
  
}