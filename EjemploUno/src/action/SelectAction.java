package action;
import com.opensymphony.xwork2.ActionSupport;
import java.util.*;

public class SelectAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private List<String> dataList = null;

	@Override
	public String execute() throws Exception {
		dataList = new ArrayList<String>();
		dataList.add("Cuenta 1y");
		dataList.add("Cuenta 2");
		dataList.add("Cuenta 3");
		dataList.add("Cuenta 4");
		return "SUCCESS_2";
	}

	public List<String> getDataList() {
		return dataList;
	}

	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}
}